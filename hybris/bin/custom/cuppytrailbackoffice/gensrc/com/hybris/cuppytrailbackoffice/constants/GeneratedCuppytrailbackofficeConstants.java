/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 1, 2019 5:32:46 PM                      ---
 * ----------------------------------------------------------------
 */
package com.hybris.cuppytrailbackoffice.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedCuppytrailbackofficeConstants
{
	public static final String EXTENSIONNAME = "cuppytrailbackoffice";
	
	protected GeneratedCuppytrailbackofficeConstants()
	{
		// private constructor
	}
	
	
}
