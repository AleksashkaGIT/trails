/**
 * 
 */
package de.hybris.platform.cuppy.daos;

import java.util.List;

import de.hybris.platform.cuppy.model.CompetitionModel;
import de.hybris.platform.cuppy.model.GroupModel;


/**
 * @author andreas.thaler
 * 
 */
public interface GroupDao
{
	List<GroupModel> findGroupByName(final CompetitionModel competition, final String name, final String langIso);

	/**
	 * Tested.
	 */
	List<GroupModel> findGroups(CompetitionModel competition);

	/**
	 * Tested.
	 */
	List<GroupModel> findGroupByCode(CompetitionModel competition, final String code);

	List<GroupModel> findAllGroups();
}
