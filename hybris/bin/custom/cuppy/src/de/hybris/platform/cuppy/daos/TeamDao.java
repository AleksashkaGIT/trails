/**
 * 
 */
package de.hybris.platform.cuppy.daos;

import java.util.List;

import de.hybris.platform.cuppy.model.TeamModel;


/**
 * @author andreas.thaler
 * 
 */
public interface TeamDao
{
	List<TeamModel> findTeamByName(final String name, final String langIso);
	List<TeamModel> findAllTeams();
}
