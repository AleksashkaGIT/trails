package de.hybris.platform.cuppytrail.services.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.cuppy.model.NewsModel;
import de.hybris.platform.cuppy.model.PlayerModel;
import de.hybris.platform.cuppy.services.impl.DefaultMatchService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CuppytrailMatchService extends DefaultMatchService {

    @Autowired
    private SessionService sessionService;

    @Autowired
    private CatalogVersionService catalogVersionService;

    @Override
    public List<NewsModel> getLatestNews(PlayerModel player, int count) {
        return (List<NewsModel>) this.sessionService.executeInLocalView(new SessionExecutionBody() {
            @Override
            public Object execute() {
                CuppytrailMatchService.this.catalogVersionService.setSessionCatalogVersion("Default", "Online");
                return CuppytrailMatchService.super.getLatestNews(player, count);
            }
        });
    }
    public void setSessionService(final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void setCatalogVersionService(final CatalogVersionService catalogVersionService){
        this.catalogVersionService = catalogVersionService;
    }
}
