package de.hybris.platform.cuppytrail.interceptors;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import de.hybris.platform.cuppytrail.events.CapacityEvent;
import de.hybris.platform.cuppytrail.model.StadiumModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import static de.hybris.platform.servicelayer.model.ModelContextUtils.getItemModelContext;

public class StadiumValidatePrepareInterceptor implements ValidateInterceptor, PrepareInterceptor
{

    public static final int BIG_STADIUM = 50000;

    public static final int TOO_BIG_STADIUM = 100000;

    @Autowired
    private EventService eventService;

    @Override
    public void onPrepare(final Object model, InterceptorContext interceptorContext) throws InterceptorException {
        if (model instanceof StadiumModel) {
            final StadiumModel stadium = (StadiumModel) model;
            if (hasBecomeBig(stadium, interceptorContext)) {
                eventService.publishEvent(new CapacityEvent(stadium.getCapacity(), stadium.getCode()));
            }
        }
    }

    @Override
    public void onValidate(final Object model, final InterceptorContext interceptorContext) throws InterceptorException {
        if (model instanceof StadiumModel) {
            final StadiumModel stadium = (StadiumModel) model;
            final Integer capacity = stadium.getCapacity();
            if (capacity != null && capacity.intValue() >= TOO_BIG_STADIUM) {
                throw new InterceptorException("Capacity is too high");
            }
            Date foundingDate = stadium.getFoundingDate();
            if (foundingDate != null && foundingDate.before(new Date()))
            {
                throw new InterceptorException("Date of the stadium foundation can not be earlier than current date!");
            }
        }
    }

    private boolean hasBecomeBig(StadiumModel stadium, InterceptorContext context) {
        final Integer capacity = stadium.getCapacity();
        if (capacity != null && capacity.intValue() >= BIG_STADIUM && capacity.intValue() <= TOO_BIG_STADIUM) {
            if (context.isNew(stadium)) {
                return true;
            } else {
                final Integer oldValue = getItemModelContext(stadium).getOriginalValue(StadiumModel.CAPACITY);
                if (oldValue == null || oldValue.intValue() < 50000) {
                    return true;
                }
            }
        }
        return false;
    }
}
