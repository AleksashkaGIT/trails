/**
 * ***********************************************************************
 * Copyright (c) 2017, SAP <sap.com>
 *
 * All portions of the code written by SAP are property of SAP.
 * All Rights Reserved.
 *
 * SAP
 *
 * Moscow, Russian Federation
 *
 * Web: sap.com
 * ***********************************************************************
 */
package de.hybris.platform.cuppytrail.jobs;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.cuppy.model.GroupModel;
import de.hybris.platform.cuppy.model.MatchModel;
import de.hybris.platform.cuppy.model.TeamModel;
import de.hybris.platform.cuppy.services.MatchService;
import de.hybris.platform.cuppytrail.model.StadiumModel;
import de.hybris.platform.cuppytrail.services.StadiumService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

public class CreateNewMatchJob extends AbstractJobPerformable<CronJobModel>
{
	@Autowired
	private ModelService modelService;

	@Autowired
	private StadiumService stadiumService;

	@Autowired
	private MatchService matchService;

	private int matchesCount;

	private KeyGenerator matchIdGenerator;

	@Override
	public PerformResult perform(final CronJobModel cronJobModel)
	{
		List<StadiumModel> stadiums = stadiumService.getStadiums();
		List<GroupModel> groups = matchService.getAllGroups();
		List<TeamModel> teams = matchService.getAllTeams();
		matchesCount = (int) (Math.random() * 5 + 1);
		for (int i = 0; i < matchesCount; i++)
		{
			MatchModel newMatch = modelService.create(MatchModel.class);
			newMatch.setDate(DateUtils.addMonths(new Date(), 1));
			newMatch.setId(Integer.parseInt(matchIdGenerator.generate().toString()));
			newMatch.setMatchday((int) (Math.random() * 10 + 1));
			newMatch.setStadium(stadiums.get((int)(Math.random() * stadiums.size())));
			newMatch.setGroup(groups.get((int)(Math.random() * groups.size())));
			newMatch.setHomeTeam(teams.get((int)(Math.random() * teams.size())));
			newMatch.setGuestTeam(teams.get((int)(Math.random() * teams.size())));
			modelService.save(newMatch);
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	@Override
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public void setStadiumService(final StadiumService stadiumService)
	{
		this.stadiumService = stadiumService;
	}

	@Required
	public void setMatchIdGenerator(final KeyGenerator matchIdGenerator)
	{
		this.matchIdGenerator = matchIdGenerator;
	}
}
