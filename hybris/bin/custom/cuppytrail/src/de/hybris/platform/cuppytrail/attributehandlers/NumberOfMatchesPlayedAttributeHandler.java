package de.hybris.platform.cuppytrail.attributehandlers;

import de.hybris.platform.cuppy.model.MatchModel;
import de.hybris.platform.cuppytrail.model.StadiumModel;
import de.hybris.platform.cuppytrail.services.StadiumService;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Component
public class NumberOfMatchesPlayedAttributeHandler extends AbstractDynamicAttributeHandler<Integer, StadiumModel> {
    @Autowired
    private StadiumService stadiumService;

    @Override
    public Integer get(final StadiumModel model)
    {
        try {
            StadiumModel st = stadiumService.getStadiumForCode(model.getCode());
            Collection<MatchModel> matches = st.getMatches();
            List<MatchModel> playedMatches = new ArrayList<>();
            for (MatchModel match : matches) {
                if (match.getDate().before(new Date())) {
                    playedMatches.add(match);
                }
            }
            return playedMatches.size();
        } catch (final IllegalStateException e) {
            return null;
        }
    }

    public void setStadiumService(final StadiumService stadiumService) {
        this.stadiumService = stadiumService;
    }
}
