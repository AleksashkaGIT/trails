package de.hybris.platform.cuppytrail.facades.impl;

import de.hybris.platform.cuppytrail.data.StadiumData;
import de.hybris.platform.cuppytrail.facades.StadiumFacade;
import de.hybris.platform.cuppytrail.model.StadiumModel;
import de.hybris.platform.cuppytrail.services.StadiumService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.beans.factory.annotation.Required;

import java.util.ArrayList;
import java.util.List;

public class DefaultStadiumFacade implements StadiumFacade {

    private StadiumService stadiumService;

    private Converter<StadiumModel, StadiumData> stadiumConverter;

    @Override
    public List<StadiumData> getStadiums(final String format) {
        final List<StadiumModel> stadiumModels = stadiumService.getStadiums();
        final List<StadiumData> stadiumData = new ArrayList<StadiumData>();
        String urlImg;
        for (final StadiumModel stadium : stadiumModels) {
            StadiumData data = stadiumConverter.convert(stadium);
            try {
                urlImg = stadiumService.getImageUrlFromStadium(stadium, format);
            } catch (final Exception e) {
                urlImg = ""; // something bad happened, possibly no image available
            }
            data.setImageUrl(urlImg);
            stadiumData.add(data);
        }
        return stadiumData;
    }

    @Override
    public StadiumData getStadium(final String name, final String format) {
        StadiumModel stadium = null;
        if (name != null) {
            stadium = stadiumService.getStadiumForCode(name);
            if (stadium == null) {
                return null;
            }
        } else {
            throw new IllegalArgumentException("Stadium with name " + name + " not found.");
        }
        String urlBigImg;
        try {
            urlBigImg = stadiumService.getImageUrlFromStadium(stadium, format);
        } catch (final Exception e) {
            urlBigImg = "";
        }
        StadiumData data = stadiumConverter.convert(stadium);
        data.setImageUrl(urlBigImg);
        return data;
    }

    @Required
    public void setStadiumService(final StadiumService stadiumService) {
        this.stadiumService = stadiumService;
    }

    @Required
    public void setStadiumConverter(final Converter<StadiumModel, StadiumData> stadiumConverter) {
        this.stadiumConverter = stadiumConverter;
    }

    public Converter<StadiumModel, StadiumData> getStadiumConverter() {
        return stadiumConverter;
    }

    public StadiumService getStadiumService() {
        return stadiumService;
    }
}