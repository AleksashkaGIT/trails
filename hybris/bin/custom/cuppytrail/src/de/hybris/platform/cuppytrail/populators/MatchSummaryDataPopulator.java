package de.hybris.platform.cuppytrail.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.cuppy.model.MatchModel;
import de.hybris.platform.cuppytrail.data.MatchSummaryData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.DateFormat;
import java.util.Date;

public class MatchSummaryDataPopulator implements Populator<MatchModel, MatchSummaryData> {
    @Override
    public void populate(MatchModel source, MatchSummaryData target) throws ConversionException {
        final String homeTeam = source.getHomeTeam().getName();
        final String guestTeam = source.getGuestTeam().getName();
        final Date matchDate = source.getDate();
        final String formattedDate = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT).format(matchDate);
        final String guestGoals = source.getGuestGoals() == null ? "-" : source.getGuestGoals().toString();
        final String homeGoals = source.getHomeGoals() == null ? "-" : source.getHomeGoals().toString();
        final String matchSummaryFormatted = homeTeam + ":( " + homeGoals + " ) " + guestTeam + " ( " + guestGoals + " ) "
                + formattedDate;
        target.setHomeTeam(homeTeam);
        target.setGuestTeam(guestTeam);
        target.setDate(matchDate);
        target.setGuestGoals(guestGoals);
        target.setHomeGoals(homeGoals);
        target.setMatchSummaryFormatted(matchSummaryFormatted);
    }
}


