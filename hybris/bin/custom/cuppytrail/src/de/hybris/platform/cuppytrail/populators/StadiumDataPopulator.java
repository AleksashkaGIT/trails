package de.hybris.platform.cuppytrail.populators;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.cuppy.model.MatchModel;
import de.hybris.platform.cuppytrail.data.MatchSummaryData;
import de.hybris.platform.cuppytrail.data.StadiumData;
import de.hybris.platform.cuppytrail.model.StadiumModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import org.springframework.beans.factory.annotation.Required;

public class StadiumDataPopulator implements Populator<StadiumModel, StadiumData> {

    private Converter<MatchModel, MatchSummaryData> matchDataConverter;

    @Override
    public void populate(StadiumModel source, StadiumData target) throws ConversionException {
        target.setName(source.getCode());
        if (source.getCapacity() != null) {
            target.setCapacity(source.getCapacity().toString());
        }
        if (source.getMatches() != null) {
            target.setMatches(Converters.convertAll(source.getMatches(), matchDataConverter));
        }
        if (source.getFoundingDate() != null) {
            target.setFoundingDate(source.getFoundingDate());
        }
    }

    public Converter<MatchModel, MatchSummaryData> getMatchDataConverter(){
        return matchDataConverter;
    }

    @Required
    public void setMatchDataConverter(final Converter<MatchModel, MatchSummaryData> matchDataConverter) {
        this.matchDataConverter = matchDataConverter;
    }
}