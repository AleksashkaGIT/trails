package de.hybris.platform.cuppytrail.interceptors;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.hybris.platform.cuppy.model.NewsModel;
import de.hybris.platform.cuppytrail.model.StadiumModel;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import static junit.framework.TestCase.*;
import static org.fest.assertions.Assertions.assertThat;

public class StadiumInterceptorIntegrationTest extends ServicelayerTest
{
	@Resource
	private ModelService modelService;
	@Resource
	private FlexibleSearchService flexibleSearchService;

	@SuppressWarnings("unused")
	static final private Logger LOG = Logger.getLogger(StadiumInterceptorIntegrationTest.class);

	@Before
	public void setUp() throws Exception
	{
		createCoreData();
		createDefaultCatalog();
	}

	@Test
	public void testValidationInterceptor()
	{
		//given
		final StadiumModel stadium = modelService.create(StadiumModel.class);
		stadium.setCode("invalid");
		stadium.setCapacity(Integer.valueOf(200000));
		//when
		try
		{
			modelService.save(stadium);
			Assert.fail();
		}
		//then
		catch (final ModelSavingException e)
		{
			assertThat(e.getCause().getClass()).isEqualTo(InterceptorException.class);
			assertThat(e.getMessage()).contains("Capacity is too high");
		}
	}

	@Test
	public void testEventSending() throws Exception
	{
		//given
		final StadiumModel stadium = modelService.create(StadiumModel.class);
		final Random rnd = new Random();
		final String code = "stadium(" + System.currentTimeMillis() + "|" + rnd.nextInt() + ")";
		stadium.setCode(code);
		stadium.setCapacity(Integer.valueOf(80000));
		//when
		modelService.save(stadium);
		//then
		Thread.sleep(4000);
		assertThat(findLastNews().getContent(Locale.ENGLISH)).contains(code);
	}

	@Test
	public void foundingDateValidTest() throws ParseException
	{
		final StadiumModel stadium = modelService.create(StadiumModel.class);
		stadium.setCode("test");
		stadium.setCapacity(100);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date testDate = format.parse("2019-03-01");
		stadium.setFoundingDate(testDate);
		modelService.save(stadium);
		StadiumModel savedStadium = findStadiumModel();
		assertEquals(stadium, savedStadium);
		assertNotNull(savedStadium);
	}

	@Test(expected = ModelSavingException.class)
	public void foundingDateInvalidTest() throws ParseException
	{
		final StadiumModel stadium = modelService.create(StadiumModel.class);
		stadium.setCode("test2");
		stadium.setCapacity(1002);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date oldDate = format.parse("2015-03-01");
		stadium.setFoundingDate(oldDate);
		modelService.save(stadium);
		assertNull(findStadiumModel());
	}

	private NewsModel findLastNews()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("SELECT {n:").append(NewsModel.PK).append("} ");
		builder.append("FROM {").append(NewsModel._TYPECODE).append(" AS n} ");
		builder.append("WHERE ").append("{n:").append(NewsModel.COMPETITION).append("} IS NULL ");
		builder.append("ORDER BY ").append("{n:").append(NewsModel.CREATIONTIME).append("} DESC");

		final List<NewsModel> list = flexibleSearchService.<NewsModel>search(builder.toString()).getResult();
		if (list.isEmpty())
		{
			return null;
		}
		else
		{
			return list.get(0);
		}
	}

	private StadiumModel findStadiumModel()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("SELECT {n:").append(StadiumModel.PK).append("} ");
		builder.append("FROM {").append(StadiumModel._TYPECODE).append(" AS n} ");
		final List<StadiumModel> list = flexibleSearchService.<StadiumModel>search(builder.toString()).getResult();
		if (list.isEmpty())
		{
			return null;
		}
		else
		{
			return list.get(0);
		}
	}
}
