package de.hybris.platform.cuppytrail.facades.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cuppytrail.data.StadiumData;
import de.hybris.platform.cuppytrail.model.StadiumModel;
import de.hybris.platform.cuppytrail.services.StadiumService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@UnitTest
public class DefaultStadiumFacadeUnitTest {
    private DefaultStadiumFacade stadiumFacade;
    private final static String STADIUM_NAME = "wembley";
    private final static Integer STADIUM_CAPACITY = Integer.valueOf(12345);
    private final static String IMAGE_FORMAT = "dummyFormat";
    public Date foundingDate = DateUtils.addYears(new Date(), 1);
    private List<StadiumModel> stadiums = new ArrayList<StadiumModel>();
    private List<StadiumData> stadiumsDTO = new ArrayList<StadiumData>();
    private StadiumService stadiumService;

    private Converter<StadiumModel, StadiumData> stadiumConverter = new Converter<StadiumModel, StadiumData>()
    {
        @Override
        public StadiumData convert(final StadiumModel stadiumModel) throws ConversionException
        {
            return stadiumsDTO.get(0);
        }

        @Override
        public StadiumData convert(final StadiumModel stadiumModel, final StadiumData stadiumData) throws ConversionException
        {
            return stadiumsDTO.get(0);
        }
    };

    @Before
    public void setUp() {
        final StadiumModel wembley = new StadiumModel();
        wembley.setCode(STADIUM_NAME);
        wembley.setCapacity(STADIUM_CAPACITY);
        wembley.setFoundingDate(foundingDate);
        stadiums.add(wembley);
        final StadiumData stadiumData = new StadiumData();
        stadiumData.setName(STADIUM_NAME);
        stadiumData.setCapacity(STADIUM_CAPACITY.toString());
        stadiumData.setFoundingDate(foundingDate);
        stadiumsDTO.add(stadiumData);
        stadiumFacade = new DefaultStadiumFacade();
        stadiumService = mock(StadiumService.class);
        stadiumFacade.setStadiumService(stadiumService);
    }

    @Test
    public void testGetAllStadiums() {
        final StadiumModel wembley = stadiums.get(0);

        when(stadiumService.getStadiums()).thenReturn(stadiums);
        when(stadiumConverter.convert(wembley)).thenReturn(stadiumsDTO.get(0));
        final List<StadiumData> dto = stadiumFacade.getStadiums(IMAGE_FORMAT);
        assertNotNull(dto);
        assertEquals(stadiums.size(), dto.size());
        assertEquals(wembley.getCode(), dto.get(0).getName());
        assertEquals(wembley.getCapacity().toString(), dto.get(0).getCapacity());
        assertEquals(wembley.getFoundingDate(), dto.get(0).getFoundingDate());
    }

    @Test
    public void testGetStadium() {
        final StadiumModel wembley = stadiums.get(0);
        when(stadiumService.getStadiumForCode("wembley")).thenReturn(wembley);
        when(stadiumConverter.convert(wembley)).thenReturn(stadiumsDTO.get(0));
        final StadiumData stadium = stadiumFacade.getStadium("wembley", IMAGE_FORMAT);
        assertEquals(wembley.getCode(), stadium.getName());
        assertEquals(wembley.getCapacity().toString(), stadium.getCapacity());
        assertEquals(wembley.getFoundingDate(), stadium.getFoundingDate());
    }
}
