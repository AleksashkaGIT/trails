package de.hybris.platform.cuppytrail.attributehandlers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cuppy.model.MatchModel;
import de.hybris.platform.cuppytrail.model.StadiumModel;
import de.hybris.platform.cuppytrail.services.StadiumService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@UnitTest
public class NumberOfMatchesPlayedAttributeUnitTest {
    private StadiumModel stadium;
    private StadiumService service;
    private NumberOfMatchesPlayedAttributeHandler handler;

    @Before
    public void setUp() throws ParseException {
        service = Mockito.mock(StadiumService.class);
        handler = new NumberOfMatchesPlayedAttributeHandler();
        handler.setStadiumService(service);
        stadium = new StadiumModel();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date testDate = format.parse("2018-03-01");
        MatchModel matchModel = new MatchModel();
        matchModel.setHomeGoals(1);
        matchModel.setGuestGoals(2);
        matchModel.setDate(testDate);
        List<MatchModel> matches = new ArrayList<>();
        matches.add(matchModel);
        stadium.setCode("test");
        stadium.setFoundingDate(testDate);
        stadium.setCapacity(1000);
        stadium.setMatches(matches);
    }

    @Test
    public void testValidHandler(){
        Mockito.reset(service);
        Mockito.when(service.getStadiumForCode("test")).thenReturn(stadium);
        Mockito.when(stadium.getNumberOfMatchesPlayed()).thenReturn(Integer.valueOf(1));
        assertEquals(Integer.valueOf(1), handler.get(stadium));
    }

    @Test(expected = IllegalStateException.class)
    public void testInvalidHandler(){
        Mockito.reset(service);
        Mockito.when(service.getStadiumForCode("test").getNumberOfMatchesPlayed()).thenThrow(new IllegalStateException());
        assertNull(handler.get(stadium));
    }
}
