package de.hybris.platform.cuppytrail.validation;

import de.hybris.platform.cuppy.model.GroupModel;
import de.hybris.platform.cuppy.model.MatchBetModel;
import de.hybris.platform.cuppy.model.MatchModel;
import de.hybris.platform.cuppy.model.PlayerModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.validation.exceptions.HybrisConstraintViolation;
import de.hybris.platform.validation.services.ValidationService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Set;

public class ValidationTest extends ServicelayerTransactionalTest {
    @Resource
    private ModelService modelService;

    @Resource
    private ValidationService validationService;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/impex/essentialdataValidationConstraints.impex", "utf-8");
    }

    @Test
    public void testMatchBetConstraints() {
        final MatchBetModel matchBetModel = modelService.create(MatchBetModel.class);
        matchBetModel.setPlayer((PlayerModel) modelService.create(PlayerModel.class));
        matchBetModel.setMatch((MatchModel) modelService.create(MatchModel.class));
        matchBetModel.setHomeGoals(50);
        matchBetModel.setGuestGoals(45);
        Set<HybrisConstraintViolation> violations = validationService.validate(matchBetModel);
        Assert.assertTrue("The violation set cannot be null or empty", violations != null && violations.size() > 0);
        Assert.assertEquals("There should be two constraint violations", 2, violations.size());
        for (final HybrisConstraintViolation hybrisConstraintViolation : violations) {
            System.out.println(hybrisConstraintViolation.getProperty() + ":" + hybrisConstraintViolation.getLocalizedMessage());
        }
    }

    @Test
    public void testNotEmptyConstraint() {
        final GroupModel groupModel = modelService.create(GroupModel.class);
        final Set<HybrisConstraintViolation> violations = validationService.validate(groupModel);
        Assert.assertTrue("The violation set cannot be null or empty", violations != null && violations.size() > 0);
        Assert.assertEquals("There should be one constraint violation", 1, violations.size());
        for (final HybrisConstraintViolation hybrisConstraintViolation : violations) {
            System.out.println(hybrisConstraintViolation.getProperty() + ": " + hybrisConstraintViolation.getLocalizedMessage());
        }
    }
}
